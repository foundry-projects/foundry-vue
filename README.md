<div align=center>

# Foundry - Vue (Beta)

<img title="Minimum core version" src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/foundry-projects/foundry-vue/-/raw/master/src/module.json&label=core&query=minimumCoreVersion&suffix=%2B&style=flat-square&color=important">
<img title="Latest compatible version" src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/foundry-projects/foundry-vue/-/raw/master/src/module.json&label=compatible&query=compatibleCoreVersion&style=flat-square&color=important">
<img src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/foundry-projects/foundry-vue/-/raw/master/package.json&label=version&query=version&style=flat-square&color=success">

[![Roadmap](https://img.shields.io/badge/roadmap-trello-blue?style=flat-square&logo=trello)](https://trello.com/b/vWFk8aWr/nick-east-foundry-vtt-projects)
[![Chat on Discord](https://img.shields.io/discord/520640779534729226?style=flat-square&label=discord&logo=discord)](https://discord.gg/59Tz2X7)
[![Twitter Follow](https://img.shields.io/badge/follow-%40NickEastNL-blue.svg?style=flat-square&logo=twitter)](https://twitter.com/NickEastNL)
[![Become a Patron](https://img.shields.io/badge/support-patreon-orange.svg?style=flat-square&logo=patreon)](https://www.patreon.com/nick_east)
[![Donate via Ko-Fi](https://img.shields.io/badge/support-ko--fi-ff4646?style=flat-square&logo=ko-fi)](https://ko-fi.com/nickeast)

</div>

This module adds support for using the Vue library with systems and modules developed for Foundry VTT. You only need to install and enable this module if you use a system or module created using Vue.

## Usage

You can install this module using either the manifest link (`https://gitlab.com/foundry-projects/foundry-vue/-/raw/master/src/module.json`), or the in-app installer.

Once installed, you must enable this module every time you create a new world that uses a system or module created with Vue. If not enabled, you will not be able to use anything created with it. To double-check, use `F12` to open the Developer Tools in your browser, select the `Console` tab, and look for the following error (you may need to scroll up):

`Uncaught ReferenceError: FoundryVue is not defined`

If present, you have not properly enabled this module. Once enabled, the error should be gone.

### For developers

When developing a system or module using Vue, ensure you read the documentation on the [wiki](https://gitlab.com/foundry-projects/foundry-vue/-/wikis/Home). Using this module for development requires at least a basic to intermediary understanding of Vue. I cannot provide help with Vue itself, and the Foundry Discord should remain free from lengthy discussions. Please read the official Vue [documentation](https://vuejs.org/v2/guide/) for more information.

**ATTENTION:** Using Vue adds a dependency to systems and modules. It is **_recommended_** to only use Vue for systems, and modules that are compatible only with those systems.

Installing the module in your workspace allows you to use type definitions. Simply run the following in your workspace:

`npm install -D gitlab:foundry-projects/foundry-vue`

If the module has been updated, simply run the above command again to update the installed package.

## Support

This module is provided as-is. It will be updated as necessary, but aside from bugfixes I cannot manage specific feature requests beyond Foundry's core functionality. By extension, Merge Requests will also be ignored.

Compatibility issues are also a risk, when multiple systems or modules are installed that require different versions of this module. Care is taken as much as possible to ensure deprecated features are not removed and system and module developers have time to update their projects to the latest stable version.