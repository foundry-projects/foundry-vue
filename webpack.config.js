const path = require('path');
const fs = require('fs-extra');
const { VueLoaderPlugin } = require('vue-loader');

const babelConfig = fs.readJSONSync(path.join(process.cwd(), '.babelrc'));

module.exports = {
	entry: {
		FoundryVue: './src/index.ts',
	},
	output: {
		path: path.join(process.cwd(), 'dist'),
		publicPath: '/',
		filename: '[name].js',
		library: 'FoundryVue',
		libraryTarget: 'window',
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
			},
			{
				test: /\.ts$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							...babelConfig,
						},
					},
					{
						loader: 'ts-loader',
						options: {
							appendTsSuffixTo: [/\.vue$/],
						},
					},
				],
			},
		],
	},
	plugins: [new VueLoaderPlugin()],
	resolve: {
		alias: {
			vue$: 'vue/dist/vue.esm.js',
		},
		extensions: ['.js', '.ts', '.json', '.vue'],
		modules: ['node_modules'],
	},
};
