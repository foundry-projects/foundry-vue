// ------------------------------------ //
// FOUNDRY - VUE						//
// v1.0.0								//
// ------------------------------------ //

import Vue from 'vue';

import { VueApplication } from './foundry/VueApplication';
import { VueBaseSheet } from './foundry/VueBaseSheet';
import { VueItemSheet } from './foundry/VueItemSheet';
import { VueActorSheet } from './foundry/VueActorSheet';
import { registerComponents } from './register';

// Explicitly enable Vue DevTools support so it works in production
Vue.config.devtools = true;

Hooks.once('init', function() {
	console.log('Vue | Initializing Foundry Vue');
	console.log(
		'Vue | If you are developing using Vue, you should download the Vue DevTools: https://github.com/vuejs/vue-devtools'
	);

	registerComponents();
});

export * from 'vue-property-decorator';
export * from './mixins';
export * from './components';

export { Vue, VueApplication, VueBaseSheet, VueItemSheet, VueActorSheet };
