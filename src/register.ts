import Vue from 'vue';

export function registerComponents() {
	// @ts-ignore
	const components = require.context('./components', false, /F[A-Z]\w+\.(vue)$/);

	components.keys().forEach((fileName: string) => {
		const config = components(fileName);

		const name = fileName
			.replace(/^\.\//, '')
			.replace(/\.\w+$/, '')
			.match(/[A-Z]{1,}(?=[A-Z][a-z]*|\b)|[A-Z]+[a-z]*/g)
			.map(x => x.toLowerCase())
			.join('-');

		Vue.component(name, config.default || config);
	});
}
