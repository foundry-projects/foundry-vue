import { Vue, Component, Prop } from 'vue-property-decorator';
import { VueApplicationOptions } from '../foundry/VueApplication';

/**
 * Makes a component draggable
 *
 * Add the `calculatedPos` value to the outer window's `style` binding
 *
 * Bind `onDragMouseDown` to the drag anchor
 */
@Component
export class Draggable extends Vue {
	@Prop() options: VueApplicationOptions;
	@Prop() position: ApplicationPosition;

	calculatedPos: {
		left: string;
		top: string;
	} = {
		left: null,
		top: null,
	};

	calculatedSize: {
		width: string;
		height: string;
	} = {
		width: null,
		height: null,
	};

	startDragPos: {
		left: number;
		top: number;
		dragX: number;
		dragY: number;
	} = {
		left: null,
		top: null,
		dragX: null,
		dragY: null,
	};

	startDragSize: {
		width: number;
		height: number;
		dragX: number;
		dragY: number;
	} = {
		width: null,
		height: null,
		dragX: null,
		dragY: null,
	};

	isMoving = false;
	isResizing = false;

	mounted() {
		const el = this.$el as HTMLElement;
		const width = this.options.width;
		const height = this.options.height;
		let left = this.options.left;
		let top = this.options.top;

		this.setSize(width, height);

		// Calculate starting position after
		// computing starting size
		this.$nextTick().then(() => {
			if (!left) {
				left = (window.innerWidth - el.offsetWidth) / 2;
			}
			if (!top) {
				top = (window.innerHeight - el.offsetHeight) / 2;
			}

			this.setPosition(left, top);
		});
	}

	setPosition(left: number, top: number) {
		const el = this.$el as HTMLElement;
		const p = this.position;

		const maxLeft = Math.max(window.innerWidth - el.offsetWidth, 0);
		p.left = Math.clamped(left, 0, maxLeft);

		const maxTop = Math.max(window.innerHeight - el.offsetHeight, 0);
		p.top = Math.clamped(top, 0, maxTop);

		this.calculatedPos.left = p.left + 'px';
		this.calculatedPos.top = p.top + 'px';
	}

	setSize(width: number | string, height: number | string) {
		const el = this.$el as HTMLElement;
		const p = this.position;
		const pop = this.options.popOut;
		const styles = window.getComputedStyle(el);

		if (height === 'auto' || this.options.height === 'auto') {
			height = null;
		}

		if (width === 'auto' || this.options.width === 'auto') {
			width = null;
		}

		const minWidth = parseInt(styles.minWidth) || (pop ? MIN_WINDOW_WIDTH : 0);
		p.width = Math.clamped(
			minWidth,
			(width as number) || el.offsetWidth,
			parseInt(el.style.maxWidth) || window.innerWidth
		);

		const minHeight = parseInt(styles.minHeight) || (pop ? MIN_WINDOW_HEIGHT : 0);
		p.height = Math.clamped(
			minHeight,
			(height as number) || el.offsetHeight,
			parseInt(el.style.maxHeight) || window.innerHeight
		);

		// Update position after calculating new size, if necessary
		this.$nextTick().then(() => {
			if ((width as number) + p.left > window.innerWidth) {
				this.setPosition(p.left, p.top);
			}

			if ((height as number) + p.top > window.innerHeight) {
				this.setPosition(p.left, p.top);
			}
		});

		this.calculatedSize.width = p.width + 'px';
		this.calculatedSize.height = p.height + 'px';
	}

	onResizeMouseDown(ev: MouseEvent) {
		const el = this.$el as HTMLElement;
		this.startDragSize = {
			width:
				this.position.width === 'auto'
					? el.clientWidth
					: (this.position.width as number),
			height:
				this.position.height === 'auto'
					? el.clientHeight
					: (this.position.height as number),
			dragX: ev.clientX,
			dragY: ev.clientY,
		};
		this.isResizing = true;
		window.addEventListener('mousemove', this.onResizeMouseMove.bind(this), false);
		window.addEventListener('mouseup', this.onResizeMouseUp.bind(this), false);
	}

	onResizeMouseMove(ev: MouseEvent) {
		if (!this.isResizing) return;
		this.setSize(
			this.startDragSize.width + (ev.clientX - this.startDragSize.dragX),
			this.startDragSize.height + (ev.clientY - this.startDragSize.dragY)
		);
	}

	onResizeMouseUp() {
		this.isResizing = false;
		window.removeEventListener('mousemove', this.onResizeMouseMove, false);
		window.removeEventListener('mouseup', this.onResizeMouseUp, false);
	}

	onDragMouseDown(ev: MouseEvent) {
		this.startDragPos = {
			left: this.position.left,
			top: this.position.top,
			dragX: ev.clientX,
			dragY: ev.clientY,
		};
		this.isMoving = true;
		window.addEventListener('mousemove', this.onDragMouseMove.bind(this), false);
		window.addEventListener('mouseup', this.onDragMouseUp.bind(this), false);
	}

	onDragMouseMove(ev: MouseEvent) {
		if (!this.isMoving) return;
		this.setPosition(
			this.startDragPos.left + (ev.clientX - this.startDragPos.dragX),
			this.startDragPos.top + (ev.clientY - this.startDragPos.dragY)
		);
	}

	onDragMouseUp() {
		this.isMoving = false;
		window.removeEventListener('mousemove', this.onDragMouseMove, false);
		window.removeEventListener('mouseup', this.onDragMouseUp, false);
	}
}
