import { Vue, Component, Prop } from 'vue-property-decorator';

@Component
export class Droppable extends Vue {
	@Prop({ required: true }) data: any;

	mounted() {
		(this.$el as HTMLElement).addEventListener('dragover', this._onDragOver);
		(this.$el as HTMLElement).addEventListener('drop', this._onDrop);
	}

	private _onDragItemStart(ev: DragEvent) {
		const li = ev.currentTarget as HTMLElement;
		const item = this.data.actor.getOwnedItem(li.dataset.itemId);
		const dragData = {
			type: 'Item',
			actorId: this.data.actor.id,
			tokenId: undefined,
			data: item.data,
		};
		if (this.data.actor.isToken) dragData.tokenId = this.data.actor.token.id;
		ev.dataTransfer.setData('text/plain', JSON.stringify(dragData));
	}

	private _onDragOver(ev: DragEvent) {
		ev.preventDefault();
		return false;
	}

	private async _onDrop(ev: DragEvent) {
		ev.preventDefault();
		console.log('Drop', this.data);

		let data;
		try {
			data = JSON.parse(ev.dataTransfer.getData('text/plain'));
			console.log(data);
			if (data.type !== 'Item') return;
		} catch (err) {
			return false;
		}

		// Case 1 - Import from a Compendium pack
		const actor = this.data.actor;
		if (data.pack) {
			return actor.importItemFromCollection(data.pack, data.id);
		}

		// Case 2 - Data explicitly provided
		else if (data.data) {
			let sameActor = data.actorId === actor._id;
			if (sameActor && actor.isToken) sameActor = data.tokenId === actor.token.id;
			if (sameActor) return this._onSortItem(event, data.data);
			// Sort existing items
			else return actor.createEmbeddedEntity('OwnedItem', duplicate(data.data)); // Create a new Item
		}

		// Case 3 - Import from World entity
		else {
			const item = game.items.get(data.id);
			if (!item) return;
			return actor.createEmbeddedEntity('OwnedItem', duplicate(item.data));
		}
	}

	/**
	 * Handle a drop event for an existing Owned Item to sort that item
	 * @param {Event} event
	 * @param {Object} itemData
	 * @private
	 */
	_onSortItem(event, itemData) {
		if (this.data.actor.isToken) return;

		// Get the drag source and its siblings
		const source = this.data.actor.getOwnedItem(itemData._id);
		const siblings = this._getSortSiblings(source);

		// Get the drop target
		const dropTarget = event.target.closest('.item');
		const targetId = dropTarget ? dropTarget.dataset.itemId : null;
		const target = siblings.find(s => s.data._id === targetId);

		// Ensure we are only sorting like-types
		if (target && source.data.type !== target.data.type) return;

		// Perform the sort
		const sortUpdates = SortingHelpers.performIntegerSort(source, {
			target: target,
			siblings,
		});
		const updateData = sortUpdates.map(u => {
			const update = u.update;
			update._id = u.target.data._id;
			return update;
		});

		// Perform the update
		return this.data.actor.updateEmbeddedEntity('OwnedItem', updateData);
	}

	/* -------------------------------------------- */

	_getSortSiblings(source) {
		return this.data.actor.items.filter(
			i => i.data.type === source.data.type && i.data._id !== source.data._id
		);
	}
}
