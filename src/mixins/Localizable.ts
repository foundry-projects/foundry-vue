import { Vue, Component, Prop } from 'vue-property-decorator';

/**
 * Adds localization to a component
 */
@Component
export class Localizable extends Vue {
	localize(key: string) {
		return game.i18n.localize(key);
	}
}
