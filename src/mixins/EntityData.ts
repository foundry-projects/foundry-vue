import { Vue, Component, Prop } from 'vue-property-decorator';

/**
 * Adds common functionality to all sheet components
 */
@Component
export class EntityData extends Vue {
	@Prop({ required: true }) data: any;

	get options() {
		return this.data.options;
	}

	get entity() {
		return this.data.entity;
	}

	get entityData() {
		return this.entity.data;
	}

	submit() {
		this.data.submit(this.entity);
	}
}
