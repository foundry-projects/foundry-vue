import { VueBaseSheet } from './VueBaseSheet';

export class VueItemSheet extends VueBaseSheet {
	object: Item;

	constructor(...args) {
		super(...args);
		if (this.actor) {
			this.actor.apps[this.appId] = this;
		}
	}
	get id() {
		if (this.actor) return `actor-${this.actor.id}-item-${this.item.id}`;
		else return super.id;
	}

	get item() {
		return this.object;
	}

	/**
	 * The Actor instance which owns this item. This may be null if the item is unowned.
	 * @type {Actor}
	 */
	get actor() {
		return this.item.actor;
	}

	protected _getHeaderButtons() {
		const buttons = super._getHeaderButtons();
		const canConfigure = this.isEditable && game.user.isGM;
		if (!canConfigure) return buttons;

		// Add a Sheet Configuration button
		buttons.unshift({
			label: 'Sheet',
			class: 'configure-sheet',
			icon: 'fas fa-cog',
			onclick: (ev: Event) => this._onConfigureSheet(ev),
		});
		return buttons;
	}

	/**
	 * Handle requests to configure the default sheet used by this Item
	 * @private
	 */
	protected _onConfigureSheet(event: Event) {
		event.preventDefault();
		new EntitySheetConfig(this.item, {
			top: this.position.top + 40,
			left: this.position.left + ((this.position.width as number) - 400) / 2,
		}).render(true);
	}
}
