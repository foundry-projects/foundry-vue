import Vue, { VueConstructor } from 'vue';
import { gsap } from 'gsap';
import FAppWindow from '../components/FAppWindow.vue';

export interface VueApplicationOptions extends ApplicationOptions {
	/** The Vue component to render for this Application */
	component?: string | VueConstructor;
}

/**
 * A base Application class that replaces Foundry's template rendering
 * with Vue components.
 */
export class VueApplication extends Application {
	options: VueApplicationOptions;

	vue: Vue;
	vueData: any;

	constructor(options?: VueApplicationOptions) {
		super(options);
		this.vueData = {
			windowData: null,
			data: null,
			position: null,
		};
	}

	/**
	 * The main Vue component to render
	 */
	get component() {
		return this.options.component;
	}

	/**
	 * Component to render as the outer window
	 */
	get window() {
		return FAppWindow;
	}

	get template() {
		return '';
	}

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ['sheet'],
		});
	}

	get title() {
		return this.options.title;
	}

	/**
	 * For a Vue Application, add the base data values that the Vue instance
	 * needs. Computations should be handled in the rendered Vue components
	 */
	getData() {
		this.vueData.data = {
			options: this.options,
		};

		return this.vueData.data;
	}

	/**
	 * Handles creating the Vue instance and triggering changes to the data
	 * from Foundry
	 */
	async _render(force = false, options: any = {}) {
		//-- CORE --//
		// Do not render under certain conditions
		const states = Application.RENDER_STATES;
		if ([states.CLOSING, states.RENDERING].includes(this._state)) return;
		if (!force && this._state <= states.NONE) return;
		if ([states.NONE, states.CLOSED].includes(this._state)) {
			console.log(`${vtt} | Rendering ${this.constructor.name}`);
		}
		this._state = states.RENDERING;
		//-- END CORE --//

		//-- VUE --//
		// Return early if no component is associated with the Application
		if (!this.component) {
			console.error(`Vue | Component not defined in ${this.constructor.name}`);
			ui.notifications.error(
				`Vue | Component not defined in ${this.constructor.name}`
			);
			return;
		}

		// Get the data for the hooks. Also triggers an update to the reactive data
		// so the Vue instance can update itself
		const data = await this.getData();

		const classes = options.classes || this.options.classes;
		this.vueData.windowData = {
			id: this.id,
			classes: classes.join(' '),
			appId: this.appId,
			title: this.title,
			headerButtons: this._getHeaderButtons(),
			isMinimized: this._minimized,
		};

		// Create the Vue instance when first created
		if (!this.vue) {
			let componentName: string;
			if (typeof this.component === 'string') {
				componentName = this.component;
			} else {
				componentName = this.component['name'];
			}
			console.log(`Vue | Rendering component '${componentName}'`);

			this._createDummy();
			ui.windows[this.appId] = this;

			this.vueData.position = this.position;

			// Create the Vue instance
			this.vue = new Vue({
				el: `#vue-${this.appId}`,
				data: {
					window: this.window,
					component: this.component,
					vueData: this.vueData,
				},
				render: function(createElement) {
					return createElement(
						this.window,
						{
							props: {
								data: this.vueData.windowData,
								options: this.vueData.data.options,
								position: this.vueData.position,
							},
						},
						[
							createElement(this.component, {
								props: { data: this.vueData.data },
							}),
						]
					);
				},
			});

			gsap.fromTo(this.vue.$el, { opacity: 0 }, { duration: 0.2, opacity: 1 });
		}
		//-- END VUE --//

		//-- CORE --//
		// Dispatch Hooks for rendering the base and subclass applications
		const base = this.options.baseApplication;
		if (base && base !== this.constructor.name) {
			Hooks.call(`render${base}`, this, null, data);
		}
		Hooks.call('render' + this.constructor.name, this, null, data);
		this._state = states.RENDERED;
		//-- END CORE --//
	}

	/**
	 * Creates a dummy element for this Application to inject Vue into
	 */
	protected _createDummy() {
		$('body').append(`<div id="vue-${this.appId}"></div>`);
	}

	async close() {
		const states = Application.RENDER_STATES;
		if (this._state !== states.RENDERED) return;
		this._state = states.CLOSING;

		// Get the element
		const el = this.vue.$el as HTMLElement;
		el.style.minHeight = '0';
		el.style.overflow = 'hidden';

		// Dispatch Hooks for closing the base and subclass applications
		const base = this.options.baseApplication;
		if (base && base !== this.constructor.name) {
			Hooks.call(`close${base}`, this, el);
		}
		Hooks.call('close' + this.constructor.name, this, el);

		gsap.to(el, {
			duration: 0.2,
			height: 0,
			onComplete: () => {
				el.remove();

				// Clean up data
				this._element = null;
				delete ui.windows[this.appId];
				this._minimized = false;
				this._scrollPositions = null;
				this._state = states.CLOSED;

				this.vue = null;
			},
		});
	}
}
