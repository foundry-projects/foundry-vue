import { VueBaseSheet } from './VueBaseSheet';

export class VueActorSheet extends VueBaseSheet {
	object: Actor;
	token: Token;

	constructor(...args) {
		super(...args);

		/**
		 * If this Actor Sheet represents a synthetic Token actor, reference the active Token
		 * @type {Token}
		 */
		this.token = this.object.token;
	}

	/**
	 * Default rendering and configuration options used for the ActorSheet and its subclasses.
	 * See Application.defaultOptions and FormApplication.defaultOptions for more details.
	 * @type {Object}
	 */
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			height: 720,
			width: 800,
			closeOnSubmit: false,
			submitOnClose: true,
			submitOnChange: true,
			resizable: true,
			baseApplication: 'ActorSheet',
		});
	}

	/**
	 * Define a unique and dynamic element ID for the rendered ActorSheet application
	 */
	get id(): string {
		const actor = this.actor;
		let id = `actor-${actor.id}`;
		if (actor.isToken) id += `-${actor.token.id}`;
		return id;
	}

	/**
	 * The displayed window title for the sheet - the entity name by default
	 */
	get title() {
		return this.token && !this.token.data.actorLink
			? `[Token] ${this.actor.name}`
			: this.actor.name;
	}

	/**
	 * A convenience reference to the Actor entity
	 */
	get actor() {
		return this.object;
	}

	getData() {
		const data = super.getData();
		this.vueData.data = {
			...data,
			actor: this.actor,
		};
	}

	async _render(force = false, options: any = {}) {
		if (force) this.token = options.token || null;
		return super._render(force, options);
	}

	_getHeaderButtons() {
		let buttons = super._getHeaderButtons();

		// Token Configuration
		const canConfigure =
			game.user.isGM || (this.actor.owner && game.user.can('TOKEN_CONFIGURE'));
		if (this.options.editable && canConfigure) {
			buttons = [
				{
					label: 'Sheet',
					class: 'configure-sheet',
					icon: 'fas fa-cog',
					onclick: ev => this._onConfigureSheet(ev),
				},
				{
					label: this.token ? 'Token' : 'Prototype Token',
					class: 'configure-token',
					icon: 'fas fa-user-circle',
					onclick: ev => this._onConfigureToken(ev),
				},
			].concat(buttons);
		}
		return buttons;
	}

	/**
	 * Handle requests to configure the default sheet used by this Actor
	 */
	protected _onConfigureSheet(event) {
		event.preventDefault();
		new EntitySheetConfig(this.actor, {
			top: this.position.top + 40,
			left: this.position.left + ((this.position.width as number) - 400) / 2,
		}).render(true);
	}

	/**
	 * Handle requests to configure the prototype Token for the Actor
	 */
	protected _onConfigureToken(event) {
		event.preventDefault();

		// Determine the Token for which to configure
		const token = this.token || new Token(this.actor.data.token);

		// Render the Token Config application
		new TokenConfig(token, {
			left: Math.max(this.position.left - 560 - 10, 10),
			top: this.position.top,
			configureDefault: !this.token,
		}).render(true);
	}
}
