import { VueApplication } from './VueApplication';

export class VueBaseSheet extends VueApplication {
	object: Entity;

	constructor(object?: Entity, options?: Record<string, unknown>) {
		super(options);

		this.object = object;
		this.vueData = {
			data: null,
		};
		this.entity.apps[this.appId] = this;

		this._submit = this._submit.bind(this);
	}

	/**
	 * A convenience accessor for the object property, which in the case of a BaseEntitySheet is an Entity instance.
	 */
	get entity() {
		return this.object;
	}

	/**
	 * Whether forms are editable
	 */
	get isEditable() {
		return this.options.editable && this.entity.owner;
	}

	get title() {
		return this.entity.name;
	}

	getData() {
		const isOwner = this.entity.owner;

		this.vueData.data = {
			entity: duplicate(this.entity),
			owner: isOwner,
			limited: this.entity.limited,
			options: this.options,
			editable: this.isEditable,
			cssClass: isOwner ? 'editable' : 'locked',
			submit: this._submit,
		};

		return this.vueData.data;
	}

	protected _getHeaderButtons() {
		const buttons = super._getHeaderButtons();
		if (this.entity.compendium) {
			buttons.unshift({
				label: 'Import',
				class: 'import',
				icon: 'fas fa-download',
				onclick: async () => {
					await this.close();
					const packName = this.entity.compendium.collection;
					this.entity.collection.importFromCollection(
						packName,
						this.entity._id
					);
				},
			});
		}
		return buttons;
	}

	protected async _submit(data) {
		this.entity.update(data);
	}
}
