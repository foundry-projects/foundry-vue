import { VueApplication } from './VueApplication';
import FDialog from '../components/FDialog.vue';

export class VueDialog extends VueApplication {
	data: any;

	constructor(dialogData, options) {
		super(options);

		this.data = dialogData;
		this.options.component = FDialog;
	}

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ['dialog'],
			width: 400,
		});
	}

	get title() {
		return this.data.title || 'Dialog';
	}

	getData() {
		const data = super.getData();

		this.vueData.data = {
			...data,
			dialogData: this.data,
		};
	}
}
